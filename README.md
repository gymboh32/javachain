# Java Chain

Hash chain implementation in Java

## Blocks

Blocks are the individual elements of the hash chain.  
They consist of three parts: 
 - Heading
 - Transactions
 - Content

Each Block in the hash chain builds off of the previous block.  Each block
can be verified before it is accessed to ensure that it has not been  
tampered with or corrupted in some way.  The last known valid block is  
considered the end of the chain.  With a hash chain it is also possible to
recreate an entire sequence of changes from scratch or a known origin.  This  
means that changes can easily be rolled back, branched off, or forwarded.

### Heading

The heading holds the information of the Block, such as:
 - Hash
 - Proof
 - Previous Hash

The Hash is a SHA-256 of the content and its time of creation.

The Proof is the second number used to create a SHA-256 hash that has a  
predictable outcome when used with the proof of the previous block.

The Previous Hash is the SHA-256 of the content and the time of creation for  
the previous Block.

### Transaction

-- TODO: This part needs work/clarification but it should give visibility to
any branches forward and allow for traceability and verification.

Transaction is a list of all the changes that have occured to this block.
It holds a record of the author, a timestamp, and a link to the next  
transaction.  In a way this could be another hash chain within the block.

### Content

The content is a diff between the content of the previous block and the  
expected final content.  Don't confuse this for the content itself, it is  
more of a way of getting to the final result based on the previous content.


