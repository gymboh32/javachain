package org.ragecastle;

import java.security.MessageDigest;

public class Block {

    private String hash;
    private String name;
    private Integer proof = 0;
    private Block next;
    private Block prev;

    private boolean isProofValid (Integer prevProof, Integer newProof) {
        byte [] bytes;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(prevProof.byteValue());
            md.update(newProof.byteValue());
            bytes = md.digest();
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return false;
        }
        return bytes.toString().endsWith("0000");
    }

    private Integer proofOfWork (Integer prevProof) {
        Integer proof = 0;
        while (!isProofValid (prevProof, proof)) {
            proof++;
        }
        return proof;

    }

    public Block (String name) {
        this.name = name;
        this.proof = proofOfWork(1);
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(name.getBytes());
            md.update(proof.byteValue());
            this.hash = md.digest().toString();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    public Block (String name, Integer prevProof) {
        this.name = name;
        this.proof = proofOfWork(prevProof);
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(name.getBytes());
            md.update(proof.byteValue());
            this.hash = md.digest().toString();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    public String getHash () {
        return this.hash.toString();
    }

    public String getName () {
        return this.name;
    }

    public Integer getProof () {
        return this.proof;
    }

    public Block getNext () {
        return this.next;
    }

    public Block getPrev () {
        return this.prev;
    }

    public void setNext (Block next) {
        this.next = next;
    }

    public void setPrev (Block prev) {
        this.prev = prev;
    }

    public boolean hasNext () {
        return this.next != null;
    }

    public boolean hasPrev () {
        return this.prev != null;
    }

    public boolean isValid () {
        boolean r;
        if (!this.hasPrev()){
            r = true;
        } else {
            r = isProofValid(prev.proof, proof);
        }
        return r;
    }
}
