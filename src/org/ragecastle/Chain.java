package org.ragecastle;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Chain {

    private Block block;

    private Block firstBlock () {
        Block cursor = this.block;
        if (cursor != null) {
            while (cursor.hasPrev()) {
                cursor = cursor.getPrev();
            }
        }
        return cursor;
    }

    private Block lastBlock () {
        Block cursor = this.block;
        if (cursor != null) {
            while (cursor.hasNext()) {
                cursor = cursor.getNext();
            }
        }
        return cursor;
    }

    public Chain (){
        this.block = null;
    }

    public Chain (String name) {
        this.block = new Block(name);
    }

    public void addBlock (String name){
        // Get to the first block in the chain
        Block cursor = firstBlock();

        // Continue if block is not null
        if (cursor != null) {
            cursor = lastBlock();
            // Create the new Block in the Chain
            Block next = new Block(name, cursor.getProof());
            // Add the new Block to the end of the Chain
            cursor.setNext(next);
            // Set the previous Block in the Chain
            next.setPrev(cursor);
            // Get back to the beginning of the Chain
            cursor = firstBlock();
            // Set this to the new Chain of Blocks
            this.block = cursor;
        } else {
            // Otherwise, create a Genesis block
            this.block = new Block(name);
        }

    }

    public JSONObject toJSON () {
        JSONObject returnJSON = new JSONObject();
        JSONArray  jsonArray  = new JSONArray();
        // Get to the start of the chain
        Block cursor = firstBlock();

        // Walk down the chain
        while (cursor != null) {
            // Create a JSON Object from the Block data
            JSONObject jsonBlock = new JSONObject();
            jsonBlock.put("Hash",  cursor.getHash());
            jsonBlock.put("Name",  cursor.getName());
            jsonBlock.put("Proof", cursor.getProof());
            // Add the JSON Object the the Chain (JSON Array)
            jsonArray.add(jsonBlock);
            // Move to the next block
            cursor = cursor.getNext();
        }

        // Return a single JSON Object containing the chain
        returnJSON.put("Chain", jsonArray);
        return returnJSON;
    }
}
