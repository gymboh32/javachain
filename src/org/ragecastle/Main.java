package org.ragecastle;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
	    Chain chain = new Chain();

	    chain.addBlock("Block_1");
        chain.addBlock("Block_2");
        chain.addBlock("Block_3");
        chain.addBlock("Block_4");
        chain.addBlock("Block_5");
        chain.addBlock("Block_6");
        printChain (chain.toJSON());
    }

    private static void printChain (JSONObject jsonObject) {
        JSONArray chain = (JSONArray) jsonObject.get("Chain");

        Iterator<JSONObject> iterator = chain.iterator();
        do {
            JSONObject block = iterator.next();
            System.out.println ("HASH: " + block.get("Hash"));
            System.out.println ("  NAME : " + block.get("Name"));
            System.out.println ("  PROOF: " + block.get("Proof"));
        } while (iterator.hasNext());
    }
}
